import requests
import datetime
import rfc3339
import iso8601
import json
import matplotlib as plt
import numpy as np
import pandas as pd

class GlassnodeClient:
  def __init__(self):
    self._api_key = ''

  @property
  def api_key(self):
    return self._api_key

  @api_key.setter
  def api_key(self, value):
    self._api_key = value

  def get(self, url, a=None, s=None, u=None, i='24h', c='native'):
    p = dict()
    if a is not None:
      p['a'] = a
    if s is not None:
      p['s'] = iso8601.parse_date(s).strftime('%s')
    if u is not None:
      p['u'] = iso8601.parse_date(u).strftime('%s')
    p['i'] = i
    p['c'] = c
    p['api_key'] = self.api_key

    r = requests.get(url, params=p)
    r.raise_for_status()
    return r.json()

  @staticmethod
  def pretty_print(m):
      l = list()
      for v in m:
        ts = datetime.datetime.fromtimestamp(v['t'], datetime.timezone.utc)
        d = { 't': rfc3339.rfc3339(ts) }
        for key,value in v.items():
          if key == 't':
            continue
          d[key] = value
        l.append(d)
      print(json.dumps(l, indent=2))

glassnode = GlassnodeClient()
